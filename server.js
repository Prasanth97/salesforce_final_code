const express = require("express");
const { ObjectID } = require("mongodb");
const app = express();
const mongoose = require("mongoose");
const model = require('./customerModel')
require('dotenv').config()
require('dotenv/config');

app.use(express.json())

app.listen(process.env.PORT, () => {
          mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true,useUnifiedTopology: true }, (error, client) => {
              if(error) {
                  throw error;
              }
            });
           });
//===============================Post Data===================================//      
app.post('/post_data',  async(req,res) => {

     var detail = ({
       CreatedDate : req.body.CreatedDate,
       Customer_Name : req.body.Customer_Name,
       Customer_Mobile : req.body.Customer_Mobile,
       Email_id : req.body.Email_id ,
       Enquiry_For : req.body.Enquiry_For,
       Sales_Sub_Category : req.body.Sales_Sub_Category,
       Other_Enquiry : req.body.Other_Enquiry,
       Product_Name : req.body.Product_Name,
       New_Selling_Price : req.body.New_Selling_Price,
       State : req.body.State,
       City : req.body.City,
       Location_Name : req.body.Location_Name,
       Item_Name : req.body.Item_Name,
       Current_Status : req.body.Current_Status,
       Status : req.body.Status,
       Remarks : req.body.Remarks,
       Last_Modified_Date : req.body.Last_Modified_Date,
       Languages : req.body.Languages,
       Alternate_Mobile : req.body.Alternate_Mobile,
       Plan_To_Purchase : req.body.Plan_To_Purchase,
       Profile_Name : req.body.Profile_Name,
       Nearest_Showroom_name : req.body.Nearest_Showroom_name,
       Source : req.body.Source,
       Item_Group_Name : req.body.Item_Name,
       Last_Modified_Date1 : req.body.Last_Modified_Date1
})
       const customer_data = await model.insertMany(detail) ;
         res.status(200).send(customer_data)
        })
console.log("its is working " +process.env.PORT )
    

