const express = require('express');
const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const customerDetail = new Schema({
    CreatedDate : {type : String,required : true},
    Customer_Name : {type : String,require : true},
    Customer_Mobile : {type : String,require : true},
    Email_id : {type : String,require : true},
    Enquiry_For : {type : String,require : true},
    Sales_Sub_Category : {type : String,require : true},
    Other_Enquiry : {type : String,require : true},
    Product_Name : {type : String,require : true},
    New_Selling_Price :{type : Number,require : true},
    State :{type : String,require : true},
    City :{type : String,require : true},
    Location_Name : {type : String,require : true},
    Item_Name : {type : String,require : true},
    Current_Status : {type : String,require : true},
    Status : {type : String,require : true},
    Remarks : {type : String,require : true},
    Last_Modified_Date : {type : String,require : true},
    Languages : {type : String,require : true},
    Alternate_Mobile : {type : String,require : true},
    Plan_To_Purchase : {type : String,require : true},
    Profile_Name : {type : String,require : true},
    Nearest_Showroom_name : {type : String,require : true},
    Source_name : {type : String,require : true},
    Source : {type : String,require : true},
    Item_Group_Name : {type : String,require : true},
    Last_Modified_Date1 : {type : String,require : true},
    Full_Name : {type : String,require : true}

});

module.exports = mongoose.model('salesforce', customerDetail)